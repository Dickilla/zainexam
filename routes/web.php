<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('products','ProductController');
});

// import
Route::get('/students/import', 'StudentController@import')->name('students.import');
Route::get('/student/template', 'StudentController@template')->name('students.import');

Route::get('/schools/import', 'SchoolController@import')->name('schools.import');
Route::get('/schools/templateSchool', 'SchoolController@template')->name('schools.import');
Route::post('/schools/importdata', 'SchoolController@importdata')->name('schools.importdata');

// schools
Route::get('/schools', 'SchoolController@index')->name('schools');
Route::get('/school/add', 'SchoolController@create')->name('school.create');
Route::post('/school/add', 'SchoolController@store')->name('school.store');
Route::get('school/{index_no}/edit', 'SchoolController@edit')->name('student.edit');
Route::post('school/{index_no}/update', 'SchoolController@update')->name('student.update');

// students
Route::get('/students', 'StudentController@index')->name('students');
Route::get('/student/add', 'StudentController@create')->name('student.create');
Route::post('/student/add', 'StudentController@store')->name('student.store');
Route::get('student/{admin}/edit', 'StudentController@edit')->name('student.edit');
Route::post('student/update', 'StudentController@create')->name('student.update');
Route::get('student/{admin}/delete', 'StudentController@delete')->name('student.delete');


// marks
Route::get('/marks', 'StudentMarksController@index')->name('marks');
Route::get('/mark/add', 'StudentMarksController@create')->name('mark.create');
Route::post('/mark/add', 'StudentMarksController@store')->name('mark.store');
Route::get('mark/{admin}/edit', 'StudentMarksController@edit')->name('mark.edit');
Route::post('mark/{admin}/update', 'StudentMarksController@update')->name('mark.update');
Route::get('mark/{admin}/delete', 'StudentMarksController@delete')->name('mark.delete');
Route::any('mark/search', 'StudentMarksController@search')->name('mark.search');
Route::any('mark/add/select', 'StudentMarksController@create')->name('student.select');
